public class MyTime {
    private int hour;
    private int minute;

    public MyTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }


    @Override
    public String toString() {
        return (hour<10 ? "0" : "") + hour + ":" + (minute<10 ? "0" : "") + minute;
    }

    public int incrementHour(int value) {
        int dayDiff = (hour + value) / 24;
        int newHour = (hour + value) % 24;
        if((hour + value) < 0 ) {
            dayDiff--;
            newHour += 24;
        }
        hour = newHour;
        return dayDiff;
    }

    public int incrementMinute(int value) {
        int hourDiff = (minute + value) / 60;
        minute = (minute + value) % 60;
        if((minute + value) < 0) {
            hourDiff--;
            minute += 60;
        }

        return incrementHour(hourDiff);
    }

    public boolean isBefore(MyTime time) {
        return (Integer.parseInt(toString().replace(":",""))) < (Integer.parseInt(time.toString().replace(":", "")));
    }

    public boolean isAfter(MyTime time) {
        return !isBefore(time);
    }

    public int minuteDifference(MyTime time) {

        return Math.abs((this.hour * 60 + this.minute) - (time.hour * 60 + time.minute));

    }
}