public class Rectangle {
    int sideA, sideB;
    Point topleft;

    public Rectangle(int sideA, int sideB, Point topleft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topleft = topleft;
    }

    public int area() {
        return sideA*sideB;
    }


    public int parameter() {
        return (sideA+sideB)*2;
    }


    public Point[] corners() {
        Point[] corners = new Point[4];
        corners[0]=topleft;
        corners[1]=new Point(topleft.xCoord+sideA ,topleft.yCoord+sideB);
        corners[2]=new Point(topleft.xCoord ,topleft.yCoord+sideB);
        corners[3]=new Point(topleft.xCoord+sideA ,topleft.yCoord+sideB);

        return corners;
    }
}
